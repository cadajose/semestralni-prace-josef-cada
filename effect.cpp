#include <iostream>
#include "effect.h"
void StatsEffect::Execute(Player &player) {
    std::cout << std::showpos;
    if(healthEffect != 0){
        std::cout << "health " << healthEffect << std::endl;
        player._health += healthEffect;
        if(player._health < 0) player._health = 0;
    }
    if(damageEffect != 0){
        std::cout << "damage " << damageEffect << std::endl;
        player._damage += damageEffect;
        if(player._damage < 0) player._damage = 0;
    }
    std::cout << std::noshowpos;
}