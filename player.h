#ifndef TEXT_GAME_PLAYER_H
#define TEXT_GAME_PLAYER_H

#include <list>
#include <memory>

class Item;
class Player {
public:
    void PickUp(const std::shared_ptr<Item>& item);

    const std::list<std::shared_ptr<Item>> &inventory() { return _inventory; }
    [[nodiscard]] float health() const { return _health; }
    [[nodiscard]] float damage() const { return _damage; }
    [[nodiscard]] bool dead() const { return _health == 0; }
    void takeDamage(float dmg);
private:
    std::list<std::shared_ptr<Item>> _inventory{};
    float _health = 100;
    float _damage = 5;

    friend class StatsEffect;
};


#endif //TEXT_GAME_PLAYER_H
