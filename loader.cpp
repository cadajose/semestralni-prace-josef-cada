#include "loader.h"

std::map<std::string, std::shared_ptr<Room>> Loader::rooms{};
std::map<std::string, std::shared_ptr<Item>> Loader::items{};
std::map<std::string, std::shared_ptr<Monster>> Loader::monsters{};

std::shared_ptr<Room> Loader::getRoom(const std::string &code) {
    auto it = rooms.find(code);
    if (it != rooms.end()) {
        return it->second;
    }
    std::shared_ptr<Room> room(new Room(code));
    rooms.emplace(code, room);
    return room;
}

std::shared_ptr<Item> Loader::getItem(const std::string &code) {
    auto it = items.find(code);
    if (it != items.end()) {
        return it->second;
    }
    std::shared_ptr<Item> item(new Item(code));
    items.emplace(code, item);
    return item;
}