# text-game
## Formát souborů
### Místnosti
Ve složce `rooms`
```
<název>
;description
<
    popis
>
;connections
<propojená místnost>
<propojená místnost>
...
<propojená místnost>
;items
<předmět>
<předmět>
...
<předmět>
;monsters
<monstrum>
<monstrum>
...
<monstrum>
```
### Předměty
Ve složce `items`
```
<název>
```
### Monstra
Ve složce `monsters`
```
<název>
damage <x>
health <x>
```