RPG hra, udělaná jako demoverze pro demonstraci funkčnosti. Načítaní ze souboru, místnosti, 
předměty, příšery. .exe soubor je ve slozce debug a je nutne aby byl ve stejne slozce se slozkami rooms, monsters, items

Funkce:
look - znovu prohlédne místnost 
attack - zautočí na příšeru 
go - postava jde do připojené místosti 
pickup - veme předmět
inv - ukáže inventář
stats - vypíše staty
exit - ukončí hru
