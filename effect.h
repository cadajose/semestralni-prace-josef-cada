#ifndef TEXT_GAME_EFFECT_H
#define TEXT_GAME_EFFECT_H

#include "player.h"

class Effect{
public:
    virtual void Execute(Player&player){}
};

class StatsEffect: public Effect{
public:
    void Execute (Player&player) override;
private:
    float healthEffect = 0;
    float damageEffect = 0;
    friend class Item;
};

#endif //TEXT_GAME_EFFECT_H
