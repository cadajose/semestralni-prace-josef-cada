#ifndef TEXT_GAME_ROOM_H
#define TEXT_GAME_ROOM_H

#include <string>
#include <optional>
#include <cstring>
#include <map>
#include "item.h"
#include "monster.h"

#include <vector>
#include <memory>
#include <list>

struct RoomException : std::exception {
    explicit RoomException(const std::string &code) {
        strcpy(message, "Could not load room '"); // strcat Připojí kopii zdrojového řetězce k cílovému řetězci.
        strcat(message, code.c_str());
        strcat(message, "'.");
    }
    [[nodiscard]] const char * what() const noexcept override{ // what vrací vysvetlujici string 
        return message;
    }

private:
    char message[256];
};

class RoomReference;

class Room {
public:
    explicit Room(const std::string &code);

    const std::string &name() { return _name; }

    std::shared_ptr<Room> getConnected(int n);

    std::shared_ptr<Item> takeItem(int n);

    Monster& getMonster(int n);

    int connectionCount() { return static_cast<int>(connections.size()); }

    int itemCount() { return static_cast<int>(items.size()); }

    int monsterCount() { return static_cast<int>(monsters.size());}

private:
    std::string description;
    std::string _name;
    std::vector<RoomReference> connections;
    std::list<std::shared_ptr<Item>> items;
    std::list<Monster> monsters;

    bool readFileSection(std::ifstream& file, std::string& section);
    friend std::ostream &operator<<(std::ostream &os, Room &r);
};

class RoomReference{
public:
    explicit RoomReference(std::string code)
            : code(std::move(code)) {};
    std::shared_ptr<Room> get();
private:
    const std::string code;
    std::shared_ptr<Room> room;
    void Load();

    friend std::ostream& operator<<(std::ostream& os, RoomReference& ref);
};

#endif //TEXT_GAME_ROOM_H
