#include <fstream>
#include <iostream>
#include "room.h"
#include "loader.h"

Room::Room(const std::string& code) {   // NUTNOST OPRAVIT 19. radek pacha zlo
    std::ifstream file;
    file.exceptions(std::ifstream::badbit | std::ifstream::failbit);
    try {
        file.open("rooms/" + code);
        std::getline(file, _name);
        std::string section;
        while(readFileSection(file, section));
    } catch (const ItemException & e) {
        throw e;
    } catch (const MonsterException & e){
        throw e;
    } catch (const std::exception & e) {
        throw RoomException(code);
    }
}

std::ostream& operator<<(std::ostream& os, Room& r) { // vypis veci v mistnosti
    os << r._name << std::endl << std::endl;
    os << r.description << std::endl;
    if(!r.items.empty()) {
        os << "Predmety: ";
        for (auto &it:r.items) {
            os << it->name() << " ";
        }
        os << std::endl;
    }
    if(!r.monsters.empty()) {
        os << "Monstra: ";
        for (auto &m:r.monsters) {
            os << m.name() << " ";
            if(m.dead()){
                os << "(mrtvy)";
            } else {
                os << "(" << m.health() << " HP)";
            }
            os << std::endl;
        }
    }
    os << "Propojene mistnosti: " << std::endl;
    int i = 0;
    for (auto &conn: r.connections) {
        os << i++ << " : " << conn << std::endl;
    }
    return os;
}

std::shared_ptr<Room> Room::getConnected(int n) {
    return connections[n].get();
}

Monster &Room::getMonster(int n) {
    return *std::next(monsters.begin(), n);
}

std::shared_ptr<Item> Room::takeItem(int n) {
    auto it = std::next(items.begin(), n);
    auto item = *it;
    items.erase(it);
    return item;
}

bool Room::readFileSection(std::ifstream& file, std::string& section) {// nacitani
    std::string line;
    while (!file.eof()) {
        std::getline(file, line);
        if (line.empty())
            continue;
        if (line[0] == ';') {
            section.erase();
            section.insert(0, line, 1);
            return true;
        }
        if(section == "description"){
            description.append(line);
            description.push_back('\n');
            continue;
        }
        if(section == "connections"){
            connections.emplace_back(line);
            continue;
        }
        if(section == "items"){
            items.push_back(Loader::getItem(line));
            continue;
        }
        if(section == "monsters"){
            monsters.emplace_back(line);
            continue;
        }
    }
    return false;
}

std::ostream &operator<<(std::ostream &os, RoomReference &ref) {
    ref.Load();
    os << ref.room->name();
    return os;
}

void RoomReference::Load() {
    if (!room) {
        room = Loader::getRoom(code);
    }
}

std::shared_ptr<Room> RoomReference::get() {
    Load();
    return room;
}


