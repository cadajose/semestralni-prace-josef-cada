#include "monster.h"
#include <fstream>

Monster::Monster(const std::string &code) {
    std::ifstream file;
    file.exceptions(std::ifstream::badbit | std::ifstream::failbit);
    try {
        file.open("monsters/" + code);
        std::getline(file, _name);
        std::string buf;
        float v;
        while(!file.eof()){
            file >> buf;
            file >> v;
            if(buf == "health"){
                _health = v;
            } else if(buf == "damage"){
                _damage = v;
            }
        }
    } catch (const std::exception &e) {
        throw MonsterException(code);
    }
}

void Monster::takeDamage(float dmg) {
    _health -= dmg;
    if(_health < 0) _health = 0;
}
