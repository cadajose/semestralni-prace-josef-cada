#include <string>
#include <fstream>
#include "item.h"

Item::Item(const std::string &code) {
    std::ifstream file;
    file.exceptions(std::ifstream::badbit | std::ifstream::failbit);
    try {
        file.open("items/" + code);
        std::getline(file, _name);
        std::string buf;
        file >> buf;
        if(buf == "stats"){
            auto statsEffect = new StatsEffect();
            float v;
            while(!file.eof()){
                file >> buf;
                file >> v;
                if(buf == "health"){
                    statsEffect->healthEffect = v;
                } else if(buf == "damage"){
                    statsEffect->damageEffect = v;
                }
            }
            effect = statsEffect;
        }
    } catch (const std::exception &e) {
        throw ItemException(code);
    }
}

Item::~Item() {
    delete effect;
}

void Item::Execute(Player &p) {
    if(effect == nullptr) return;
    effect->Execute(p);
}
