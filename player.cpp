#include "player.h"

void Player::PickUp(const std::shared_ptr<Item>& item) {
    _inventory.push_back(item);
}

void Player::takeDamage(float dmg) {
    _health -= dmg;
    if(_health < 0) _health = 0;
}