#ifndef TEXT_GAME_MONSTER_H
#define TEXT_GAME_MONSTER_H

#include <string>
#include <cstring>

struct MonsterException : std::exception { //výjimka k příšeře
    explicit MonsterException(const std::string &code) {
        strcpy(message, "Could not load monster '");
        strcat(message, code.c_str()); // vrátí ukazatel na pole, které obsahuje sekvenci znaků ukončenou nulou
        strcat(message, "'.");
    }

    [[nodiscard]] const char *what() const noexcept override {
        return message;
    }

private:
    char message[256];
};

class Monster { //vytváření příšery
public:
    explicit Monster(const std::string &code);
    [[nodiscard]] float health() const{ return _health; }
    [[nodiscard]] float damage() const{ return _damage; }
    [[nodiscard]] bool dead() const {return _health == 0;}
    const std::string & name() {return _name;}
    void takeDamage(float dmg);
private:
    float _health;
    float _damage;
    std::string _name;
};


#endif //TEXT_GAME_MONSTER_H
