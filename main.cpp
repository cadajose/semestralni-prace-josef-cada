#include <iostream>
#include "room.h"
#include "loader.h"
#include "player.h"

int main() {
    Player p;
    auto room = Loader::getRoom("main");
    bool exit = false;
    while (!exit) {
        std::cout << *room;
        int connectionCount = room->connectionCount();
        int itemCount = room->itemCount();
        int monsterCount = room->monsterCount();
        std::string command;
        int     param;
        while (!exit) {
            if(p.dead()){
                std::cout << "Konec hry." << std::endl;
                exit = true;
                break;
            }
            auto inv = p.inventory();
            std::cout << std::endl;
            std::cout << " go <0-" << connectionCount - 1 << "> - jit do mistnosti." << std::endl;
            if (itemCount != 0) std::cout << " pickup <0-" << itemCount - 1 << "> - sebrat predmet." << std::endl;
            std::cout << " inv - zobrazit inventar." << std::endl;
            if(!inv.empty()) std::cout << " use <0-" << inv.size()-1 << "> - pouzit predmet." << std::endl;
            std::cout << " look - rozhlednout se." << std::endl;
            if(monsterCount != 0) std::cout << " attack <0-" << monsterCount - 1 << "> - zautocit" << std::endl;
            std::cout << " stats - vypsat staty." << std::endl;
            std::cout << " exit" << std::endl;
            std::cout << ">";
            std::cout.flush();
            std::cin >> command;
            if (command == "exit") {
                exit = true;
                break;
            }
            if (command != "inv" && command != "look" && command != "stats")
                std::cin >> param;
            if (command == "go") {
                if (param < 0 || param >= connectionCount)
                    continue;
                room = room->getConnected(param);
                break;
            }
            if (command == "pickup") {
                if (param < 0 || param >= itemCount)
                    continue;
                p.PickUp(room->takeItem(param));
                continue;
            }
            if (command == "inv") {
                for (const auto &item:p.inventory()) {
                    std::cout << item->name() << std::endl;
                }
                continue;
            }
            if(command == "use"){
                if(param < 0 || param >= inv.size())
                    continue;
                auto item = std::next(inv.begin(), param);
                (*item)->Execute(p);
                inv.erase(item);
                continue;
            }
            if(command == "look"){
                std::cout << *room;
                continue;
            }
            if(command == "stats"){
                std::cout << "health " << p.health() << std::endl;
                std::cout << "damage " << p.damage() << std::endl;
                continue;
            }
            if(command == "attack"){
                if (param < 0 || param >= monsterCount)
                    continue;
                auto& monster = room->getMonster(param);
                if(monster.dead()){
                    std::cout << monster.name() << " uz je mrtvy." << std::endl;
                    continue;
                }
                monster.takeDamage(p.damage());
                if(monster.dead()){
                    std::cout << "Zabil jsi " << monster.name() << "." << std::endl;
                    continue;
                }
                std::cout << "Ubral jsi " << monster.name() << " " << p.damage() << " HP. Zbyva mu " << monster.health() << " HP." << std::endl;
                p.takeDamage(monster.damage());
                if(p.dead()){
                    std::cout << monster.name() << " tě zabil." << std::endl;
                    exit = true;
                } else {
                    std::cout << monster.name() << " ti ubral " << monster.damage() << " HP. Zbyva ti " << p.health() << " HP." << std::endl;
                }
            }
        }
    }
}
