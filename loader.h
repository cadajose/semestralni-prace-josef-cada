#ifndef TEXT_GAME_LOADER_H
#define TEXT_GAME_LOADER_H

#include "room.h"
#include "item.h"
#include "monster.h"

class Loader {
public:
    static std::shared_ptr<Room> getRoom(const std::string &code);
    static std::shared_ptr<Item> getItem(const std::string &code);

private:
    static std::map<std::string, std::shared_ptr<Room>> rooms;
    static std::map<std::string, std::shared_ptr<Item>> items;
    static std::map<std::string, std::shared_ptr<Monster>> monsters;
};

#endif //TEXT_GAME_LOADER_H
