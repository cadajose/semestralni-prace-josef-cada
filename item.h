#ifndef TEXT_GAME_ITEM_H
#define TEXT_GAME_ITEM_H

#include <cstring>
#include <string>
#include "effect.h"

struct ItemException : std::exception {
    explicit ItemException(const std::string &code) {
        strcpy(message, "Could not load item '");
        strcat(message, code.c_str());
        strcat(message, "'.");
    }

    [[nodiscard]] const char *what() const noexcept override {
        return message;
    }

private:
    char message[256];
};

class Item {
public:
    explicit Item(const std::string &code);
    void Execute(Player& p);
    const std::string &name() { return _name; }
    ~Item();
private:
    std::string _name;
    Effect* effect = nullptr;
};


#endif //TEXT_GAME_ITEM_H
